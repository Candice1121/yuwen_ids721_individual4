// Import necessary libraries and modules
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};


// Define a struct to represent the request payload received by the Lambda function
//incoming request will be "SongTitle"

#[derive(Deserialize)]
struct Request {
    #[serde(rename = "SongTitle")]
    song_title: String,
}

// Define a struct to represent the response payload to be returned by the Lambda function
#[derive(Serialize)]
struct Response {
    song_title: String,
    length: usize,
}

// The main entry point for the Lambda function, utilizing the `tokio` runtime
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    // Create a service function using the handler function and run the Lambda function
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

// The handler function responsible for processing Lambda events
async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    //count the length of the request.song_title
    let request: Request = serde_json::from_value(event.payload.clone())?;
    let length = request.song_title.len();
    // Serialize the response into JSON format and return it
    Ok(json!({
        "Popularity": length,
        "SongTitle": request.song_title,
    }))

}
