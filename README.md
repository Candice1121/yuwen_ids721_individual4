# Yuwen Ids721 Individual4

## Step Function with DynamoDB and Lambda

This project demonstrates the use of AWS Step Functions, DynamoDB, and Lambda to find a random song based on an artist name and fetch its popularity.

## Demo Video
[Youtube Link](https://youtu.be/0_xo56i9Bps)

## Prerequisites

Before running this project, make sure you have the following:

- An AWS account with appropriate permissions to create and manage Step Functions, DynamoDB tables, and Lambda functions.
- AWS CLI installed and configured with your AWS credentials.

## Preparations

1. Create a DynamoDB table to store the songs data. You can use the AWS CLI or the AWS Management Console to create the table. Make sure to note down the table name and the primary key.
![dynampdb](imgs/dynamodb.png)

2. Create two Lambda functions: one for finding a random song and another for fetching its popularity. You can use the AWS Management Console or the AWS CLI to create the functions. Make sure to note down the ARN of each function.


## Usage

1. To trigger the Step Function, you can use the AWS Management Console, AWS SDKs, or the AWS CLI. Provide the artist name as input to the Step Function.
![execute](imgs/execute.png)

2. The Step Function will execute the first Lambda function to find a random song based on the artist name. It will then execute the second Lambda function to fetch the popularity of the song.
![first_output](imgs/output_1.png)
![second_output](imgs/output_2.png)

3. The result will be displayed in the Step Function execution history.



### Update Step Function IAM Role
During create of state machine, remember to choose corresponding access for IAM roles. For my case, include lambda full access and dynamodb full access to avoid access issues.
